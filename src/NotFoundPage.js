import React from 'react';
import { Col, Row, Container, Form} from 'react-bootstrap';

const HomePage = (props) => (
    <Container >
        <Row className="justify-content-md-center">            
            <Col md="auto">
                <Form.Label>Page not found!</Form.Label>
            </Col>      
        </Row>
    </Container>
)

export default HomePage