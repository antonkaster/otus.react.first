import { createStore } from 'redux';

import { USER_ACTIONS } from './const';
import { UserLogin } from './loginAction';
import { UserRegister } from './registerAction';


const initState =
{
    user: {
        name: 'a@a.a',
        pass: 'q1w2e3'
    },
    logged: false,
    registered: false
}

export const initStore = () => {
    return createStore(mainReducer, initState);
}

const mainReducer = (state = initState, action) => {
    console.log(action);
    console.log(state);

    switch(action.type) {
        case USER_ACTIONS.LOGIN:
            return { ...state, user: action.state.user, logged: UserLogin(action.state.user) };
        case USER_ACTIONS.REGISTER:
            return { ...state, user: action.state.user, registered: UserRegister(action.state.user) };
        default:
            return state;
    }
}