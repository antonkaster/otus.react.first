import React from 'react';
import { connect } from 'react-redux';
import { Col, Row, Container, Form} from 'react-bootstrap';
import LoginForm from './Components/LoginForm';
import { USER_ACTIONS } from './Actions/const';

const LoginPage = ({user, pass, logged, dispatch}) => (
    <Container >
        <Row className="justify-content-md-center">            
            <Col md="auto">
                { logged ? 
                    <Form.Label>Welcome, {user}!</Form.Label>
                    : 
                    <Form.Label>Login, please!</Form.Label>
                }
            </Col>      
        </Row>
        <Row>      
            <Col xs="12">
                { logged ? null : 
                    <LoginForm 
                        user={user} 
                        pass={pass} 
                        submit={(state) => dispatch({ type: USER_ACTIONS.LOGIN, state: { user: { name: state.user, pass: state.pass }}})}
                        actionName="Login"
                    />
                }
            </Col>      
        </Row>
        <Row>            
        </Row>
    </Container>
)

const mapStateToProps = (state) => {
    return {
        user: state.user.name,
        pass: state.user.pass,
        logged: state.logged
    }
}

export default connect(mapStateToProps)(LoginPage)