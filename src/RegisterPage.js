import React from 'react';
import { connect } from 'react-redux';
import { Col, Row, Container, Form} from 'react-bootstrap';
import LoginForm from './Components/LoginForm';
import { USER_ACTIONS } from './Actions/const';

const RegisterPage = ({user, pass, registered, dispatch}) => (
    <Container >
        <Row className="justify-content-md-center">            
            <Col md="auto">
                { registered ? 
                    <Form.Label>You are registered as {user}!</Form.Label>
                    : 
                    <Form.Label>Register, please!</Form.Label>
                }
            </Col>      
        </Row>
        <Row>      
            <Col xs="12">
                { registered ? null : 
                    <LoginForm 
                        user={user} 
                        pass={pass} 
                        submit={(state) => dispatch({ type: USER_ACTIONS.REGISTER, state: { user: { name: state.user, pass: state.pass }}})}
                        actionName="Register"
                    />
                }
            </Col>      
        </Row>
        <Row>            
        </Row>
    </Container>
)

const mapStateToProps = (state) => {
    return {
        user: state.user.name,
        pass: state.user.pass,
        registered: state.registered
    }
}

export default connect(mapStateToProps)(RegisterPage)