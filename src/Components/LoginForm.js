import React from 'react';
import { Button,Form,Col,Row } from 'react-bootstrap';


class LoginForm extends React.Component {
    constructor(props){
        super(props);
        
        this.state = {
            user: props.user,
            pass: props.pass
        }

    }    

    render() {
        return (
            <div className="App">
                <Form onSubmit={() => this.props.submit(this.state)}>
                    <Form.Group as={Row}>
                        <Col sm="2">
                            <Form.Label>Login</Form.Label>
                        </Col>                        
                        <Col sm="10">
                            <Form.Control name="user" value={this.state.user} onChange={this.handleChange}></Form.Control>
                        </Col>                        
                    </Form.Group>
                    <Form.Group as={Row}>
                        <Form.Label column sm="2">Password</Form.Label>
                        <Col sm="10">
                            <Form.Control name="pass" type="password" value={this.state.pass} onChange={this.handleChange}></Form.Control>
                        </Col>                        
                    </Form.Group>
                    <Form.Group as={Row}>
                        <Col column sm="12">
                            <Button column sm="12" type="submit">{this.props.actionName}</Button>
                        </Col>
                    </Form.Group>
                </Form>
            </div>
        )
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit = (event) => {
        this.props.submit(this.state);
        event.preventDefault();
    }


}
export default LoginForm;